require "matrix"

class Board

  ALIVE = '*'
  DEAD = '.'

  def initialize(height, width, alive_cells)
    @height = height
    @width = width
    @number_of_elements = height * width
    @grid = Matrix.build(height, width) do
      DEAD
    end
    self.populate_with_positions(alive_cells)

  end

  def at(x, y)
    @grid[x, y]
  end

  def populate(x, y)
    @grid[x, y] = ALIVE
  end

  def kill(x, y)
    @grid[x, y] = DEAD
  end

  def as_grid
    i = 0
    @grid.each_with_index do |element, row, col|
      if row > i
        print "\n"
        i += 1
      end
      print element + " "
    end
    print "\n\n"
  end

  def is_alive_position(x, y)
    self.at(x, y) == ALIVE
  end

  def next_generation

    will_be_populated = []
    will_be_killed = []

    @grid.each_with_index do |element, row, col|

      alive_neighbors = self.count_alive_neighbors(row, col)

      will_be_populated.push([row, col]) if (!self.is_alive_position(row, col) && alive_neighbors == 3)

      will_be_killed.push([row, col]) if self.is_alive_position(row, col) && (alive_neighbors < 2 || alive_neighbors > 3)

    end

    will_be_populated.each do |new_born_cell|
      self.populate(new_born_cell[0], new_born_cell[1])
    end

    will_be_killed.each do |died_cell|
      self.kill(died_cell[0], died_cell[1])
    end

  end

  private

  def populate_with_positions(positions)

    raise ArgumentError.new(
      "Number of positions must be lower than #{@number_of_elements}, #{positions.length} given"
    ) if positions.length > @number_of_elements

    positions.each do |pos|

      x = pos['x']

      y = pos['y']

      raise ArgumentError.new('X position must not be null') if x.nil?

      raise ArgumentError.new('Y position must not be null') if y.nil?

      raise ArgumentError.new("Position height must be positive, #{x} given") if x < 0

      raise ArgumentError.new("Position width must be positive, #{y} given") if y < 0

      raise ArgumentError.new("Position height must be lower than #{@height}, #{x} given") if x > @height

      raise ArgumentError.new("Position width must be lower than #{@width}, #{y} given") if y > @width

      self.populate(x, y)
    end
  end

  def count_alive_neighbors(row, col)
    alive_neighbors = 0
    (-1..1).each do |i|
      (-1..+1).each do |j|
        if j != 0 || i != 0
          if self.is_alive_position(row + i, col + j)
            alive_neighbors += 1
          end
        end
      end
    end
    alive_neighbors
  end
end