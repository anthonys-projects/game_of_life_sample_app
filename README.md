## GOL simple implementation

Conway's Game of Life simple implementation. The software is left without any warranty!
It's just an excercise to practise the Ruby language

Notes:
<ul>
    <li>The Ruby interpreter must be installed;</li>
    <li>The project was tests under a Linux machine. It should work on other operating systems but the execution is not 100% guaranteed;</li>
</ul>

Run application (from project root)

```console
cd __PROJECT_ROOT__
chmod +x bin/appliaction
./bin/application
```

Run tests

```console
cd __PROJECT_ROOT__
ruby tests/run_all.rb
```