require_relative '../components/Board'
require 'io/console'

class GameOfLifeService

  def initialize(board)
    @board = board
  end

  def execute
    loop do
      i = 1

      loop do
        puts "Generation #{i}"
        @board.as_grid
        @board.next_generation
        i += 1

        puts 'Enter "q" to exit loop:'
        input = gets.chomp
        exit if input == 'q'
      end
    end

  end
end