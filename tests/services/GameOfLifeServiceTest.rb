require_relative '../../services/GameOfLifeService'
require_relative '../../components/Board'
require 'test/unit'

class GameOfLifeServiceTest < Test::Unit::TestCase

  def test_execute_successfully
    board = Board.new(3, 4, [{ 'x' => 0, 'y' => 1 }, { 'x' => 1, 'y' => 2}])
    game = GameOfLifeService.new(board)

    assert_instance_of(GameOfLifeService, game)
  end

end