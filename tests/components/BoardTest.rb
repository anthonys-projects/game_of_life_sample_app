require_relative '../../components/Board'
require 'test/unit'

class BoardTest < Test::Unit::TestCase

  def test_create_board_successfully
    board = Board.new(4, 3, [{ 'x' => 2, 'y' => 2 }, { 'x' => 0, 'y' => 1 }])

    assert_equal('.', board.at(0, 0))
  end

  def test_populate_successfully
    board = Board.new(3, 3, [{ 'x' => 2, 'y' => 2 }, { 'x' => 0, 'y' => 1 }])
    board.populate(0, 0)

    assert_equal('*', board.at(0, 0))
  end

  def test_is_alive_successfully
    board = Board.new(3, 3, [{ 'x' => 2, 'y' => 2 }, { 'x' => 0, 'y' => 1 }])
    board.populate(0, 0)

    assert_equal(true, board.is_alive_position(0, 0))
  end

  def test_populate_with_more_than_allowed_positions_will_throw_argument_error_exception
    assert_raise_with_message(ArgumentError, 'Number of positions must be lower than 6, 9 given') do
      Board.new(2, 3, [{ 'x' => 1, 'y' => 2 }, { 'x' => 2, 'y' => 3 }, { 'x' => 3, 'y' => 4 }, { 'x' => 4, 'y' => 5 }, { 'x' => 5, 'y' => 6 }, { 'x' => 6, 'y' => 7 }, { 'x' => 7, 'y' => 8 }, { 'x' => 8, 'y' => 9 }, { 'x' => 9, 'y' => 10 }])
    end
  end

  def test_populate_with_malformed_positions_will_throw_argument_error_exception
    assert_raise_with_message(ArgumentError, 'Y position must not be null') do
      Board.new(2, 3, [{ 'x' => 1 }])
    end
  end

  def test_populate_with_not_allowed_height_will_throw_argument_error_exception
    assert_raise_with_message(ArgumentError, 'Position height must be lower than 2, 3 given') do
      Board.new(2, 3, [{ 'x' => 3, 'y' => 1 }])
    end
  end

  def test_populate_with_negative_height_will_throw_argument_error_exception
    assert_raise_with_message(ArgumentError, 'Position height must be positive, -4 given') do
      Board.new(2, 3, [{ 'x' => -4, 'y' => 1 }])
    end
  end

  def test_populate_with_not_allowed_width_will_throw_argument_error_exception
    assert_raise_with_message(ArgumentError, 'Position width must be lower than 3, 4 given') do
      Board.new(4, 3, [{ 'x' => 3, 'y' => 4 }])
    end
  end

  # Generation 1
  # . . . . . . . . . .
  # . * . . . . . . . .
  # . * * . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .

  # Generation 2
  # . . . . . . . . . .
  # . * * . . . . . . .
  # . * * . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .

  def next_generation_successfully
    board = Board.new(5, 10, [
      { 'x' => 1, 'y' => 1 },
      { 'x' => 2, 'y' => 1 },
      { 'x' => 2, 'y' => 2 }])
    board.next_generation

    assert_equal('.', board.at(0, 0))
    assert_equal('.', board.at(1, 0))
    assert_equal('.', board.at(2, 0))
    assert_equal('.', board.at(3, 0))
    assert_equal('.', board.at(3, 1))
    assert_equal('.', board.at(3, 2))
    assert_equal('.', board.at(0, 1))
    assert_equal('.', board.at(0, 2))
    assert_equal('.', board.at(0, 3))
    assert_equal('.', board.at(1, 3))
    assert_equal('.', board.at(2, 3))
    assert_equal('.', board.at(3, 3))

    assert_equal('*', board.at(1, 1))
    assert_equal('*', board.at(2, 1))
    assert_equal('*', board.at(1, 2))
    assert_equal('*', board.at(2, 2))

  end

  # Generation 1
  # . . . . . . . . . .
  # . * * . . . . . . .
  # . * * * . . . . . .
  # . . . * . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .

  # Generation 2
  # . . . . . . . . . .
  # . * . * . . . . . .
  # . * . * . . . . . .
  # . . . * . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .
  # . . . . . . . . . .

  def test_another_next_generation_successfully
    board = Board.new(10, 10, [
      { 'x' => 1, 'y' => 1 },
      { 'x' => 2, 'y' => 1 },
      { 'x' => 1, 'y' => 2 },
      { 'x' => 2, 'y' => 2 },
      { 'x' => 2, 'y' => 3 },
      { 'x' => 3, 'y' => 3 }
    ])
    board.next_generation

    assert_equal('.', board.at(0, 0))
    assert_equal('.', board.at(1, 0))
    assert_equal('.', board.at(2, 0))
    assert_equal('.', board.at(3, 0))

    assert_equal('.', board.at(0, 1))
    assert_equal('*', board.at(1, 1))
    assert_equal('*', board.at(2, 1))

    assert_equal('.', board.at(0, 2))
    assert_equal('.', board.at(1, 2))
    assert_equal('.', board.at(2, 2))

    assert_equal('.', board.at(0, 3))
    assert_equal('*', board.at(1, 3))
    assert_equal('*', board.at(2, 3))
    assert_equal('*', board.at(3, 3))

    assert_equal('.', board.at(3, 1))
  end
end